# Simple client-server application

## Client-server communication protocol

  Client sends a command in the format:

      /<verb> <args>

  `<args>` can be any ASCII text, whilst `<verb>` has a regex pattern (0-9a-zA-Z_).

  The server response is of form:

      Command-was: <verb> <args>
      Status: <response-code>\n
      Body:\n
      <text>

  `<response-code>` is a 2-digits integer:
    - `200`, if all OK;
    - `400`, if there is a client-side error;
    - `500`, if there is a server-side error.

### Verbs
-
 `/help`
  Show a help message.

  Response body:

    FAF Server is a ...
      Usage:
        /help           -- show this message
        /hello SomeText -- show a greeting
        /time     -- get the current time (of server)
        /weather    -- get weather forecast
        /joke     -- get a random joke
        /awesome    -- get an awesome picture

  Example:

    \> /help
    Command-was: /help
    Status: 200
    Body:
    FAF Server is a ...

-
 `/hello [<name>]`
  Show a greeting message for `<name>` client.

  If optional `<name>` parameter is omitted, it defaults to `Mr. Foobar`.

  Response body:

    Hello, `<name>`. Have a nice day!\n

  Example:

    \> /hello Mike
    Command-was: /hello Mike
    Status: 200
    Body:
    Hello, Mike. Have a nice day!
    \> /hello
    Command-was: /hello
    Status: 200
    Body:
    Hello, Mr. Foobar. Have a nice day!

-
  `/time`
  Get the current server time.

  Response body:

    The current time is <day-of-week>, <date> of <month> <year>, <hour>:<minutes>:<seconds>\n.

  Example:

    \> /time
    Command-was: /time
    Status: 200
    Body:
    The current time is Wednesday, 21 of March 2018, 20:29.

-
 `/exit`
  Disconnect from server.

  Response body:

    You've disconnected.

  Example:

    \> /exit
    Command-was: /time
    Status: 200
    Body:
    You've disconnected.

-
 `/image [--path=<path>]`
  Save a image on local file-system and open it with default application. If `<path>` is not provided, the files saves in `./doc/foo` file with the corresponding extension.

  Response body:

    Extension: <extension>
    <image in base64 encoding>

  Example:

    \> /image --path=./doc/bar
    Command-was: /image --path=./doc/bar
    Status: 200
    Body:
    Extension: .png
    <image ...>
