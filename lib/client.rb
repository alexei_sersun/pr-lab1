require "socket"
require_relative "client_parser"
require_relative 'client_handler'
class Client
  def initialize(host, port)
    STDOUT.sync = true
    @server = Socket.new(:INET, :STREAM)
    socket_addr = Socket.pack_sockaddr_in(port, host)
    @server.connect(socket_addr)
    @request = nil
    @response = nil
    @client_handler = ClientHandler.new
  end

  def run
    listen
    send
    @request.join
    @response.join

  rescue Interrupt => e
    @server.close
    Thread.kill @request
    Thread.kill @response
    puts "Closed connection."
  end

  def send
    @request = Thread.new {
      loop {
        msg = $stdin.gets.chomp
        @server.puts( msg )
      }
    }
  end

  def listen
    @response = Thread.new {
      loop {
        msg = @server.recvfrom(2**16)[0].chomp
        response = ClientParser.parse(msg)
        @client_handler.handle(response)
      }
    }
  end
end