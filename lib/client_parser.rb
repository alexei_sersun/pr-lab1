class ClientParser
  def self.parse (response)
    rows = response.split("\n", 4)
    command = rows[0].split(': ', 2)[1].split(' ', 2)
    response_hash = {
      command_was: command[0],
      args: {},
      status: rows[1].split(": ", 2)[1].to_i,
      body: rows[3],
    }

    arg_regex = /^\-{2}(?<key>\D+)=(?<value>[\w.\/]+)$/
    args = {}
    command[1].split(' ').map{ |e| arg_regex.match(e) }
              .each { |e| args[e[:key]] = e[:value] if e }
    response_hash[:args] = args
    response_hash
  rescue => ex
    puts ex.backtrace
  ensure
    response_hash
  end
end