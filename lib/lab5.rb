require_relative 'server'
require_relative 'client'
require 'optparse'

options = {}
optparse = OptionParser.new { |opts|
  opts.banner = "Usage: ruby lab5.rb [--server|-s]"
  opts.on('-s', '--server', 'Run server-side script') do |server|
    options[:server] = server
  end
}.parse!

port = 8000
hostname = 'localhost'
if options[:server]
then
  Server.new(hostname, port).run
else
  Client.new(hostname, port).run
end
