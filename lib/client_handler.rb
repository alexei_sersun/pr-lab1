require "launchy"
require "base64"

class ClientHandler
  def initialize
    @statuses = {
      200 => method(:handle_ok),
      400 => method(:handle_client_error),
      500 => method(:handle_server_error),
    }

    @command_handlers = {
      '/image' => method(:handle_image)
    }
    @command_handlers.default = method(:handle_default_command)
  end

  def handle (response)
    @statuses[response[:status]].call(response)
  end
  
  def handle_ok (response)
    @command_handlers[response[:command_was]].call(response)
  end

  def handle_default_command (response)
    puts response[:body]
  end

  def handle_image (response)
    body = response[:body].split("\n", 2)
    extension = body[0].split(": ", 2)[1]
    image_data = body[1]

    if response[:args]["path"]
      path = response[:args]["path"]
    else
      path = './doc/foo'
    end

    filename = path + extension
    image = Base64.strict_decode64(image_data)
    File.open(filename, "wb") { |file| file.write(image) }
    Launchy.open(filename)
  end

  def handle_client_error (response)
    puts "Ooops, your error! The server says:"
    puts response[:body]
  end

  def handle_server_error (response)
    puts "Oops, server error! The server says:"
    puts response[:body]
  end
end