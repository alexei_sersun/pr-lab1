class ServerParser
  def self.parse (command)
    verb, args = command.strip.split(' ', 2)
    
    response_hash = {
      verb: verb,
      args: args,
    }

  rescue => ex
    puts ex.backtrace
  ensure
    response_hash
  end
end