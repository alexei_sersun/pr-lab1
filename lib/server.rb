require "socket"
require_relative 'server_parser'
require_relative 'server_handler'

class Server
  def initialize(hostname, port)
    @server = Socket.new(:INET, :STREAM)
    socket_addr = Socket.sockaddr_in(port, hostname)
    @server.bind(socket_addr)
    @server.listen(5)
    
    @connections = {clients: {}}
    @clients = {}

    @handler = ServerHandler.new

    puts "Awesome FAF Ruby server started at #{hostname} on port #{port}."
  end

  def run
    loop { 
      Thread.start(@server.accept) { |socket|
        client, address = socket
        pp client
        pp address
        @connections[:clients][address] = client
        listen_to_commands (client)
      }
    }
  rescue => ex
    puts ex
  ensure 
    disconnect_all
  end

  private

  def listen_to_commands (socket)
    loop { 
      handle_request (socket)
    }
  end

  def disconnect_all
    puts "\nServer was shut down.\n"
    @connections[:clients].each { |client| client[1].close  }
  end

  def handle_request (socket)
    raw_command = socket.gets()
    command = ServerParser.parse(raw_command)
    @handler.handle(socket, command)
  end

end
