require "levenshtein"
require "base64"

class ServerHandler
  def initialize
    @commands = {
      '/help' => method(:handle_help),
      '/hello' => method(:handle_hello),
      '/exit' => method(:handle_exit),
      '/time' => method(:handle_time),
      '/image' => method(:handle_image),
    }

    @commands.default = method(:handle_default)
  end

  def handle (socket, command)
    if !command[:verb].start_with? ('/')
      handle_not_a_command(socket, command)
    else
      handle_command(socket, command)
    end
  end

  private

  def handle_command (socket, command)
    @commands[command[:verb]].call(socket, command[:verb], command[:args])
  end

  def handle_not_a_command (socket, command)
    header = response_header(command[:verb], command[:args], 400)
    response = "#{header}Not a command. Try \"/help\" for help.\n"
    socket.write(response)
  end

  def handle_help (socket, verb, args)
    header = response_header(verb, args, 200)
    response = "#{header}FAF Server is a ...\n
    Usage:
      /help           -- show this messsage
      /hello [NAME]   -- show a greeting for NAME or Mr. Foobar
      /time           -- get the current time (of server) with a 10s delay
      /image          -- get an awesome picture
      /exit           -- exit the application\n"
    socket.write(response)
  end

  def handle_hello (socket, verb, args)
    header = response_header(verb, args, 200)
    client_name = args
    client_name = "Mr. Foobar" if client_name == nil
    response = "#{header}Hello, #{client_name}. Have a nice day!\n"
    socket.write(response)
  end

  def handle_time (socket, verb, args)
    header = response_header(verb, args, 200)
    sleep(10)
    time = Time.now.strftime("%A, %d of %B %Y, %H:%M")
    response = "#{header}The current time is #{time}.\n"
    socket.write(response)
  end

  def handle_exit (socket, verb, args)
    header = response_header(verb, args, 200)
    socket.write("#{header}You've disconnected.\n")
    disconnect(socket)
  end

  def handle_image (socket, verb, args)
    header = response_header(verb, args, 200)
    filename = './bin/logo.png'
    file = File.read(filename)
    encoded_image = Base64.strict_encode64(file)
    file_extension = File.extname(filename)
    response = "#{header}Extension: #{file_extension}\n#{encoded_image}"
    socket.write(response)
  end

  def handle_default (socket, verb, args)
    header = response_header(verb, args, 400)
    response = "#{header}Command \"#{verb}\" not found.\n"
    available_commands = @commands.keys
    suggestions = []
    available_commands.each { |e|
      if Levenshtein.distance(verb, e) < 2
        suggestions.push (e)
      end
    }

    if suggestions.length > 0
      response += "Maybe you mean:\n"
      suggestions.each { |e|
        response += " - #{e}\n"
      }
    end

    socket.write(response)
  end

  def response_header (verb, args, status)
    "Command-was: #{verb} #{args}\nStatus: #{status}\nBody:\n"
  end

  def disconnect (socket)
    printf("FAFer left %s:%s\n", socket.remote_address.ip_address, socket.remote_address.ip_port)
    socket.close
    @connections[:clients].delete(socket.remote_address)
  end
end
